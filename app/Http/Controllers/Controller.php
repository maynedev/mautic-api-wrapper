<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Mautic\Auth\ApiAuth;
use Mautic\MauticApi;
use Mockery\Exception;

class Controller extends BaseController
{

    protected $auth;
    protected $api;
    protected $mauticBaseUrl = 'https://marketing.brandfi.co.ke';

    public function __construct(Request $request)
    {
    }

    public function getAuth()
    {
        if (!isset($_SESSION['oauth']['token'])) {
            if (isset($_GET['oauth_token']))
                $_SESSION['oauth']['token'] = $_GET['oauth_token'];
        }

        $accessTokenData = array(
            'accessToken' => '1rg404s1zfy8s0s4o0ssowcgswg4s48c4804w4k4cskg484osg',
            'accessTokenSecret' => '64iuqyitjc4kkkk8kgg0s4wsoo4w0k4g0wgkos8cokg8kcgwoc',
            'accessTokenExpires' => ''
        );
        $settings = array(
            'baseUrl' => $this->mauticBaseUrl,
            'clientKey' => env('clientKey'),
            'clientSecret' => env('clientSecret'),
            'callback' => 'http://marketingapi.brandfi.co.ke/login',
            'version' => 'OAuth1a'
        );

        if (!empty($accessTokenData['accessToken']) && !empty($accessTokenData['accessTokenSecret'])) {
            $settings['accessToken'] = $accessTokenData['accessToken'];
            $settings['accessTokenSecret'] = $accessTokenData['accessTokenSecret'];
            $settings['accessTokenExpires'] = $accessTokenData['accessTokenExpires'];
        }

        $this->auth = ApiAuth::initiate($settings);
    }


    public function getToken()
    {
        $this->getAuth();
        if ($this->auth->validateAccessToken()) {
            return response()->json($this->auth->getAccessTokenData(), 200);
        } else {
            return response()->json("Invalid token", 401);
        }
    }

    public function mauticApi(Request $request)
    {
        $this->getAuth();
        $this->api = MauticApi::getContext(
            $request->context,
            $this->auth,
            $this->mauticBaseUrl . '/api/'
        );
        try {
            return response()->json(call_user_func(array($this->api, $request->action), $request->data));
        } catch (Exception $e) {
            return $e;
        }
    }
}
