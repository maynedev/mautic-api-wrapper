FROM php:7.2.5-apache
    
WORKDIR /var/www/html

# Download and Install Composer
#RUN curl -s http://getcomposer.org/installer | php \
#    && mv composer.phar /usr/local/bin/composer
#
#
#COPY composer.json composer.lock /var/www/html/

#RUN composer install --prefer-dist --optimize-autoloader --no-dev --profile -vvv
COPY . /var/www/html

# Configure directory permissions for the web server
RUN chmod -R 777 /var/www/html/

RUN a2enmod rewrite

COPY virtualhost.conf /etc/apache2/sites-enabled/000-default.conf

